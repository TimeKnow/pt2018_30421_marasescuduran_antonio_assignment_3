package PT2018.HW1_Tema3.company.presentation;

import PT2018.HW1_Tema3.company.businesslogic.Product;
import PT2018.HW1_Tema3.company.dataaccess.AbstractDAO;
import PT2018.HW1_Tema3.company.dataaccess.ProductDAO;

import java.util.List;

public class TableViewControllerProduct extends TableViewController<Product> {


    public TableViewControllerProduct() {
        ProductDAO productDAO = new ProductDAO();
        this.setAbstractDAO(productDAO);

    }
    @SuppressWarnings("Duplicates")
    public void refresh(){
        int currentRowCount = getTableView().getModel().getRowCount();
        for(int i=currentRowCount-1;i>=0;i--)
            this.getTableView().getModel().removeRow(i);

        List<Product> products = this.getAbstractDAO().findAll();
        for(Product p : products){
            List<Object[]> dataProduct = AbstractDAO.retrieveProperties(p);
            Object[] values = new Object[dataProduct.size()];
            int k=0;
            for(Object[] o : dataProduct)
                values[k++]=o[1];
            this.getTableView().getModel().addRow(values);
        }
        this.getTableView().getModel().addRow(new Object[getTableView().getColumnsLength()]);
    }

    @Override
    public int insert(Object data) {
        return 0;
    }


}
