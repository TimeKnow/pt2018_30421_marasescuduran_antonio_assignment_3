package PT2018.HW1_Tema3.company.presentation;

import PT2018.HW1_Tema3.company.businesslogic.Client;
import PT2018.HW1_Tema3.company.businesslogic.Orders;
import PT2018.HW1_Tema3.company.businesslogic.Product;
import PT2018.HW1_Tema3.company.dataaccess.AbstractDAO;
import PT2018.HW1_Tema3.company.dataaccess.ClientDAO;
import PT2018.HW1_Tema3.company.dataaccess.OrdersDAO;
import PT2018.HW1_Tema3.company.dataaccess.ProductDAO;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

public class TableViewControllerOrders extends TableViewController<Orders> {

    public TableViewControllerOrders() {
        this.setAbstractDAO(new OrdersDAO());
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void refresh() {
        int currentRowCount = getTableView().getModel().getRowCount();
        for(int i=currentRowCount-1;i>=0;i--)
            this.getTableView().getModel().removeRow(i);

        List<Orders> orders = this.getAbstractDAO().findAll();
        for(Orders order : orders){
            List<Object[]> dataProduct = AbstractDAO.retrieveProperties(order);
            Object[] values = new Object[dataProduct.size()];
            int k=0;
            for(Object[] o : dataProduct)
                values[k++]=o[1];
            this.getTableView().getModel().addRow(values);
        }
        this.getTableView().getModel().addRow(new Object[getTableView().getColumnsLength()]);
    }

    @Override
    public int insert(Object data) {

        Orders order = (Orders)data;
        if(order==null)
            return -1;

        Product p = (new ProductDAO()).findById(order.getId_Product());
        Client c = (new ClientDAO()).findById(order.getId_Client());
        if(p==null || c==null)
            return -1;

        if(p.getQuantity()-order.getQuantity()<0)
            return -2;
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("programLog.txt", true));
            String text = "Bill ID "+order.getId()+"\nClient Name: "+c.getName() + "\nAddress: " + c.getAddress() + "\nEmail: "+c.getEmail()+"\n";
            text+="Product: "+p.getName() + "\nValue: "+p.getValue()+"\n";
            text+="Total Value: " +p.getValue()*order.getQuantity()+"\n\n";
            writer.write(text);
            writer.close();
            p.setQuantity(p.getQuantity()-order.getQuantity());
            (new ProductDAO()).update(p);
        }
        catch (Exception e){
            System.out.println("Bill Error!");
        }
        return 0;
    }


}
