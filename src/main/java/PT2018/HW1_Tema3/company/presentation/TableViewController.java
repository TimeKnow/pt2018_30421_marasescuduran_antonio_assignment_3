package PT2018.HW1_Tema3.company.presentation;

import PT2018.HW1_Tema3.company.dataaccess.AbstractDAO;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

public abstract class TableViewController<E> implements ActionListener,ListSelectionListener {


    private AbstractDAO<E> abstractDAO;
    private TableView tableView;
    private final Class<E> type;

    @SuppressWarnings("unchecked")
    public TableViewController(){
        this.type = (Class<E>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }

    public void setTableView(TableView tableView) {
        this.tableView = tableView;
    }

    private E createElement(int row){
        try {
            E element = type.newInstance();

            int k=0;
            for(Field field : type.getDeclaredFields())
            {
                field.setAccessible(true);
                Object value = tableView.getModel().getValueAt(row,k);
                Object test=null;
                try {
                    test = Integer.valueOf(String.valueOf(value));
                }catch (NumberFormatException e){
                    try {
                        test = Float.parseFloat(String.valueOf(value));
                    }
                    catch (NumberFormatException e1){

                    }
                }
                if(test!=null)
                    value = test;
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(),type);
                Method method = propertyDescriptor.getWriteMethod();
                method.invoke(element,value);
                k++;
            }

            return element;
        }
        catch (Exception e){
            System.out.println("Invalid Data");
        }
        return null;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(tableView.getButtonList().get(0)==e.getSource())
            insertEvent();
        if(tableView.getButtonList().get(1)==e.getSource())
            deleteEvent();
        if(tableView.getButtonList().get(2)==e.getSource())
            updateEvent();
        if(tableView.getButtonList().get(3)==e.getSource())
            refreshEvent();
    }


    @Override
    public void valueChanged(ListSelectionEvent e) {

    }

    public abstract void refresh();

    public abstract int insert(Object data);

    public void insertEvent(){
        E element = createElement(tableView.getModel().getRowCount()-1);
        if(element==null){
            JOptionPane.showMessageDialog(null,"Invalid field data!!");
            return;
        }
        int abstrIns = insert(element);
        switch (abstrIns){
            case -1:JOptionPane.showMessageDialog(null,"Invalid field data!!");return;
            case -2: JOptionPane.showMessageDialog(null,"Under Stock!!");return;
            default:break;
        }
        abstractDAO.insert(element);
        refresh();
    }
    @SuppressWarnings("Duplicates")
    public void deleteEvent(){
        int selectedRow = this.tableView.getTable().getSelectedRow();
        if(selectedRow == -1) {
            JOptionPane.showMessageDialog(null, "Please select an element!");
            return;
        }

        if(selectedRow==tableView.getModel().getRowCount()-1){
            JOptionPane.showMessageDialog(null,"Please select another element!");
            return;
        }
        int idRow = Integer.valueOf(String.valueOf(this.tableView.getModel().getValueAt(selectedRow,0)));
        if(abstractDAO.delete(idRow)==false)
        {
            JOptionPane.showMessageDialog(null,"Delete failed!");
            return;
        }

        this.tableView.getModel().removeRow(selectedRow);
    }
    @SuppressWarnings("Duplicates")
    public void updateEvent(){
        int rowCount = this.tableView.getModel().getRowCount();
        for(int i=0;i<rowCount-1;i++){
            E data = createElement(i);
            if(data==null) {
                JOptionPane.showMessageDialog(null,"Update Failed!");
                return;
            }
            int abstrUpd = insert(data);
            switch (abstrUpd){
                case -1:JOptionPane.showMessageDialog(null,"Invalid field data!!");return;
                case -2: JOptionPane.showMessageDialog(null,"Under Stock!");return;
                default:break;
            }
            abstractDAO.update(data);
        }
        this.refresh();
    }

    public void refreshEvent(){
        refresh();
    }

    public void findByIdEvent(){
        System.out.println("eva");
    }

    public void setAbstractDAO(AbstractDAO<E> abstractDAO) {
        this.abstractDAO = abstractDAO;
    }

    public AbstractDAO<E> getAbstractDAO() {
        return abstractDAO;
    }

    public TableView getTableView() {
        return tableView;
    }

}
