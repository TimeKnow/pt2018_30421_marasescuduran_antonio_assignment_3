package PT2018.HW1_Tema3.company.presentation;

import PT2018.HW1_Tema3.company.businesslogic.Client;
import PT2018.HW1_Tema3.company.dataaccess.AbstractDAO;
import PT2018.HW1_Tema3.company.dataaccess.ClientDAO;

import java.util.List;

public class TableViewControllerClient extends TableViewController<Client> {

    public TableViewControllerClient() {
        this.setAbstractDAO(new ClientDAO());
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void refresh() {
        int currentRowCount = getTableView().getModel().getRowCount();
        for(int i=currentRowCount-1;i>=0;i--)
            this.getTableView().getModel().removeRow(i);

        List<Client> clients = this.getAbstractDAO().findAll();
        for(Client p : clients){
            List<Object[]> dataProduct = AbstractDAO.retrieveProperties(p);
            Object[] values = new Object[dataProduct.size()];
            int k=0;
            for(Object[] o : dataProduct)
                values[k++]=o[1];
            this.getTableView().getModel().addRow(values);
        }
        this.getTableView().getModel().addRow(new Object[getTableView().getColumnsLength()]);

    }

    @Override
    public int insert(Object data) {
        return 0;
    }



}
