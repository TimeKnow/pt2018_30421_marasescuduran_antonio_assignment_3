package PT2018.HW1_Tema3.company.businesslogic;

public class Supplier {
     private int id;
     private String name;
     private int cif;
     private float turnover;

    public Supplier() {
    }

    public Supplier(int id, String name, int cif, float turnover) {
        this.id = id;
        this.name = name;
        this.cif = cif;
        this.turnover = turnover;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCif() {
        return cif;
    }

    public void setCif(int cif) {
        this.cif = cif;
    }

    public float getTurnover() {
        return turnover;
    }

    public void setTurnover(float turnover) {
        this.turnover = turnover;
    }

    @Override
    public String toString() {
        return "Supplier{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cif='" + cif + '\'' +
                ", turnover=" + turnover +
                '}';
    }
}
