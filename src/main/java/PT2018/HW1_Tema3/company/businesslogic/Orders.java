package PT2018.HW1_Tema3.company.businesslogic;

public class Orders {
    private int id;
    private int Id_Client;
    private int Id_Product;
    private int quantity;

    public Orders() {
    }


    public Orders(int id, int idClient, int idProduct, int quantity) {
        this.id = id;
        this.Id_Client = idClient;
        this.Id_Product = idProduct;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_Client() {
        return Id_Client;
    }

    public void setId_Client(int id_Client) {
        this.Id_Client = id_Client;
    }

    public int getId_Product() {
        return Id_Product;
    }

    public void setId_Product(int id_Product) {
        this.Id_Product = id_Product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "Id_Client=" + Id_Client +
                ", Id_Product=" + Id_Product +
                ", quantity=" + quantity +
                '}';
    }
}
