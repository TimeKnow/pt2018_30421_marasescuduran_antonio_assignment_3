package PT2018.HW1_Tema3.company.dataaccess;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AbstractDAO<E> {

    private final Class<E> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<E>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private String createSelectQuery(String field){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(this.type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    private String createInsertQuery(int fieldCount){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT ");
        sb.append(" INTO ");
        sb.append(this.type.getSimpleName());
        sb.append(" VALUES( DEFAULT, ");
        for(int i=0;i<fieldCount-1;i++)
            sb.append(" ?, ");
        sb.append("?)");
        return sb.toString();
    }

    private String createDeleteQuery(String field){
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE ");
        sb.append(" FROM ");
        sb.append(this.type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    private String createSelectAllQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(this.type.getSimpleName());
        return sb.toString();
    }

    private String createUpdateQuery(List<String> fields,String fieldType){
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(this.type.getSimpleName());
        sb.append(" SET ");

        for(int i=0;i<fields.size()-1;i++)
            sb.append(fields.get(i)+"=? ,");
        sb.append(fields.get(fields.size()-1)+"=? ");

        sb.append("WHERE " + fieldType + "=?");
        return sb.toString();
    }

    @SuppressWarnings("Duplicates")
    public boolean insert(E element){
        boolean success = false;
        List<Object[]> data = AbstractDAO.retrieveProperties(element);
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createInsertQuery(data.size()-1);
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int k=1;
            for(Object[] o : data)
                if(!String.valueOf(o[0]).equals("id"))
                {
                    if(o[1] instanceof String)
                        statement.setString(k,String.valueOf(o[1]));
                    else if(o[1] instanceof Integer)
                        statement.setInt(k,(int)o[1]);
                    else if(o[1] instanceof Float)
                        statement.setFloat(k,(float)o[1]);
                    k++;
                }
            statement.executeUpdate();
            success=true;
        }
        catch (SQLException e){
            System.out.println("Insert error!");
        }finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return success;
    }

    @SuppressWarnings("Duplicates")
    public boolean update(E element){
        boolean success = false;
        List<Object[]> data = AbstractDAO.retrieveProperties(element);
        List<String> fields = new ArrayList<>();
        for(Object[] o : data)
            if(!String.valueOf(o[0]).equals("id"))
                fields.add(String.valueOf(o[0]));

        Connection connection = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery(fields,"id");
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int k=1;
            Object[] o;
            for(int i=1;i<data.size();i++) {
                o = data.get(i);
                if (o[1] instanceof Integer)
                    statement.setInt(k, (int) o[1]);
                else if (o[1] instanceof String)
                    statement.setString(k, String.valueOf(o[1]));
                else if (o[1] instanceof Float)
                    statement.setFloat(k, (float) o[1]);
                k++;
            }
            o = data.get(0);
            statement.setInt(k,(int)o[1]);


            statement.executeUpdate();
            success=true;
        }
        catch (SQLException e){
            System.out.println("Update error!");
        }finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return success;
    }

    @SuppressWarnings("Duplicates")
    public boolean delete(int id){
        boolean success = false;
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery("id");
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            if(statement.executeUpdate()==0)return false;
            success=true;
        }
        catch (SQLException e){
            System.out.println("Delete error!");
        }finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return success;
    }

    @SuppressWarnings("Duplicates")
    public E findById(int id){

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1,id);

            resultSet = statement.executeQuery();
            if(resultSet==null)
                return null;
            List<Object>values = new ArrayList<>();
            if(resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++)
                    values.add(resultSet.getObject(i));

                int k = 0;
                E returnData = type.getDeclaredConstructor().newInstance();
                for (Field field : returnData.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    try {
                        String s = field.getName();
                        field.set(returnData, values.get(k));
                        k++;

                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                return returnData;
            }

        }
        catch (SQLException e){
            System.out.println("Select by Id error!");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    @SuppressWarnings("Duplicates")
    public List<E> findAll(){
        List<E> returnList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectAllQuery();
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);

            resultSet = statement.executeQuery();
            if(resultSet==null)
                return null;

            while(resultSet.next()) {
                List<Object>values = new ArrayList<>();
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++)
                    values.add(resultSet.getObject(i));

                int k = 0;
                E returnData = type.getDeclaredConstructor().newInstance();
                for (Field field : returnData.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    try {
                        String s = field.getName();
                        field.set(returnData, values.get(k));
                        k++;

                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                returnList.add(returnData);
            }
            return returnList;

        }
        catch (SQLException e){
            System.out.println("Select by Id error!");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return returnList;
    }


    public static List<Object[]> retrieveProperties(Object object){

        List<Object[]> returnData =new ArrayList<>();
        for(Field field : object.getClass().getDeclaredFields()){
            field.setAccessible(true);
            Object value;
            try{
                value = field.get(object);

                Object[] data = new Object[2];
                data[0]=field.getName();
                data[1]=value;
                returnData.add(data);
            }catch (IllegalArgumentException e){
                e.printStackTrace();
            }
            catch (IllegalAccessException e){
                e.printStackTrace();
            }
        }
        return returnData;
    }
}
